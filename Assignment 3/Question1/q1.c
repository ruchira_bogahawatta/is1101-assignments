#include <stdio.h>

int main () {
    float num ;
    printf("Enter a Number\n");      //Assigning float incase user inputs a decimal number
    scanf("%f",&num);
    
    if (num>0){
        printf("\nEntered number is a positive number.");      
    }
    else if (num==0){
        printf("\nEntered number is zero.");
    }
    else{
        printf("\nEntered number is a negative number.");
    }
    return 0;
}
