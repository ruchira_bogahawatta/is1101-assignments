#include <stdio.h>

int main(){
    int count=0,i,freq=0;
    char sen[1000], let;
    printf("Enter a sentence:\n");
    fgets(sen,1000,stdin);
    printf("\nEnter a letter to find the frequency\n");
    scanf("%c",&let);

    while(sen[count]!='\0'){
        count++;
    }
    for(i=0;i<count;i++){
        if(sen[i]==let){
            freq++;
        }
    }
    printf("\n\nFrequency of %c is %d \n",let,freq);
   return 0;
}
