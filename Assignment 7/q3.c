#include <stdio.h>
int main() {

  int row, col,i,j,k,tot=0;
  int mat1[50][50], mat2[50][50], sum[50][50],mul[50][50];
  printf("Addition and Multiplication of two matrices\n\n");
  printf("Enter the number of rows(Less than 50) : ");
  scanf("%d", &row);
  printf("Enter the number of columns(Less than 50) : ");
  scanf("%d", &col);
  printf("\n\nENTER THE ELEMENTS OF FIRST MATRIX:\n");
  for (i = 0; i < row;i++){
    for (j = 0; j < col;j++) {
      scanf("%d", &mat1[i][j]);
    }
  }
  printf("\n\nENTER THE ELEMENTS OF SECOND MATRIX:\n");
  for (i = 0; i < row;i++){
    for (j = 0; j < col;j++) {
      scanf("%d",&mat2[i][j]);
    }
}
  for (i=0;i<row;i++){                  //Addition
    for (j=0;j<col;j++) {
      sum[i][j]=mat1[i][j]+mat2[i][j];
    }
  }
    for(i=0;i<row;i++){                 //Multiplication
        for(j=0;j<col;j++){
            for(k=0;k<row;k++){
                tot+=mat1[i][k]*mat2[k][j];
            }
            mul[i][j]=tot;
            tot=0;
        }
    }
  printf("\nAddition of two matrices: \n\n");
  for (i=0;i<row;i++){
    for (j=0;j<col;j++) {
      printf("%d  ", sum[i][j]);
        }
        printf("\n");
    }

  printf("\nMultiplication of two matrices: \n\n");
  for (i=0;i<row;i++){
    for (j=0;j<col;j++) {
      printf("%d  ", mul[i][j]);
        }
        printf("\n");
    }
  return 0;
}
