#include <stdio.h>

int main(){
    int count=0,i,rev;
    char sen[1000], revsen[1000];
    printf("Enter a sentence to Reverse:\n");
    fgets(sen,1000,stdin);


    while(sen[count]!='\0'){
        count++;
    }
    rev=count-1;

    for(i=0;i<count;i++){
        revsen[i]=sen[rev];
        rev--;
    }
    printf("\n\nReversed sentence: %s \n",revsen);

   return 0;
}
