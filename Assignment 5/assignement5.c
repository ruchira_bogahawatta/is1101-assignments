#include<stdio.h>

int attendees(int price){
/*Formula for attendee calculation
Q = a-bp where:
a = Max. Attendees at price 0, b = change of attendees/change of price, p = price and Q = No.of Attendees
120=a-4*15
a=180*/
    return 180-4*price;
}
int totalrevenue(int price){
    return price*attendees(price);
}
int totalcost(int price){
    return 500+3*attendees(price);
}
int profit(int price){
    return totalrevenue(price)-totalcost(price);
}
int main(){
    printf("Relationship between Ticket price and Profit\n\n");
    printf(" PRICE \t\t\tPROFIT/LOSS\n\n\n");
    int price,prof,bestprice;
    int maxprofit=1;

    for(price=1;price<=45;price++){  //price range is from 1 to 45
    prof=profit(price);
    printf("  %d \t\t\t  %d\n",price,profit(price));

    if(prof>maxprofit){
        maxprofit=prof;
        bestprice=price;
    }
}

printf("\nMaximum Profit of Rs.%d is earned at a price of Rs.%d\nNumber of Attendees will be %d\n\n",maxprofit,bestprice,180-4*bestprice);

return 0;
}
