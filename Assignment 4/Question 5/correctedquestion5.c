#include <stdio.h>
int main (){

    int num;

    printf("Enter a Number: ");
    scanf("%d", &num);
    printf("\n");

    for (int count=1; count<=num; count++){
        printf("Multiplication Table for %d \n", count);

        for (int count2 = 1; count2<= 10; count2++){
            printf("%d * %d = %d\n", count, count2, count*count2);
            }

        printf("\n");
        }
    return 0;
}
