#include <stdio.h>

int main(){

struct Students {
    char firstName[50];
    char subject [50];
    int marks;
} student[10];
int num;
printf ("Student Records System\n");
printf("\nEnter the number of students: ");
scanf("%d",&num);

printf("\nEnter the student details\n");

for (int i=0;i<num;i++) {
        printf("\nStudent ID : %d\n",i+1);
        printf("Enter first name: ");
        scanf("%s", student[i].firstName);
        printf("Enter the subject: ");
        scanf("%s", student[i].subject);
        printf("Enter marks: ");
        scanf("%d", &student[i].marks);
    }
    printf("\n\nPrinting Entered Student Records\n");
    printf("------------------------------------\n");

    for (int i=0;i<num;i++) {
        printf("\nStudent ID: %d\n", i + 1);
        printf("First name: ");
        puts(student[i].firstName);
        printf("Subject: ");
        puts(student[i].subject);
        printf("Marks: %d", student[i].marks);
        printf("\n");
    }

return 0;
}
