# IS 1101- Programming and Problem Solving

1. [Assignment 1 - Introduction](https://gitlab.com/ruchira_bogahawatta/assignment1-1101)
2. [Assignment 2 - Data Types and Input/output](https://gitlab.com/ruchira_bogahawatta/is1101-assignments/-/tree/master/Assignment%202)
3. [Assignment 3 - Operators](https://gitlab.com/ruchira_bogahawatta/is1101-assignments/-/tree/master/Assignment%203)
4. [Assignment 4 - Conditions and Loops](https://gitlab.com/ruchira_bogahawatta/is1101-assignments/-/tree/master/Assignment%204)
5. [Assignment 5 - Functions](https://gitlab.com/ruchira_bogahawatta/is1101-assignments/-/tree/master/Assignment%205)
6. [Assignment 6 - Recursive Functions](https://gitlab.com/ruchira_bogahawatta/is1101-assignments/-/tree/master/Assignment%206)
7. [Assignment 7 - Arrays](https://gitlab.com/ruchira_bogahawatta/is1101-assignments/-/tree/master/Assignment%207)
8. [Assignment 8 - Structures](https://gitlab.com/ruchira_bogahawatta/is1101-assignments/-/tree/master/Assignment%208)
9. [Assignment 9 - File Handling](https://gitlab.com/ruchira_bogahawatta/is1101-assignments/-/tree/master/Assignment%209)
