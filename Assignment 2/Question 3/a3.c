#include<stdio.h>
#include<stdlib.h>

int main()
{
    int num1,num2,temp;

    printf("Enter first integer\n");
    scanf("%d",&num1);
    printf("Enter second integer\n");
    scanf("%d",&num2);

    temp=num1;
    num1=num2;
    num2=temp;

    printf("\n>>> AFTER SWAPPED <<<\n\nFirst Integer is %d\nSecond Integer is %d\n",num1,num2);


    return 0;
}
