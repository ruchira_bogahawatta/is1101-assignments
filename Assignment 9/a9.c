#include <stdio.h>
int main(){
    FILE *fp;
    char file[100];
    printf(" Creating and Writing into assignment9.txt");

    // Creating and writing file
    fp = fopen ("assignment9.txt", "w");
    fprintf(fp,"UCSC is one of the leading institutes in Sri Lanka for computing studies.");
    fclose(fp);

    // Reading the File
    fp = fopen ("assignment9.txt", "r");
    printf("\n\n Reading previously entered string in assignment9.txt :\n");

    while (fgets(file,100,fp))
    {
        printf("%s",file);}
    fclose(fp);

    // Appending to File
    fp = fopen("assignment9.txt", "a");
    fprintf(fp,"\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
    fclose(fp);

    printf("\n\n");
    return 0;
}
